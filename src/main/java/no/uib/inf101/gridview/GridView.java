package no.uib.inf101.gridview;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.IColorGrid;

public class GridView extends JPanel {
  
  private IColorGrid colorGrid;
  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;

  public GridView(IColorGrid colorGrid) {
    this.colorGrid = colorGrid;

    this.setPreferredSize(new Dimension(400, 300));
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;

    // Background
    Rectangle2D.Double backGround = new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN, getWidth() - 2*OUTERMARGIN, getHeight() - 2*OUTERMARGIN);
    g2.setColor(MARGINCOLOR);
    g2.fill(backGround);

    CellPositionToPixelConverter converter = new CellPositionToPixelConverter(backGround, colorGrid, OUTERMARGIN);

    drawCells(g2, colorGrid, converter);
  }

  private static void drawCells(Graphics2D g2, CellColorCollection cellColorCollection, CellPositionToPixelConverter converter) {
    // Creating list from cellColorCollection
    List<CellColor> cellColors = cellColorCollection.getCells();

    for (CellColor cellColor : cellColors) {

      // Gets position and color
      CellPosition pos = cellColor.cellPosition();
      Color color = cellColor.color();

      // Creates rectangle to be drawn
      Rectangle2D box = converter.getBoundsForCell(pos);

      if (color == null) {
        g2.setColor(Color.DARK_GRAY);
      }
      else {
        g2.setColor(color);
      }
      g2.fill(box);
    }
  }
}
