package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid{

  private int rows;
  private int cols;

  private Color[][] ColorGrid;

  public ColorGrid(int rows, int cols) {
    this.rows = rows;
    this.cols = cols;
    this.ColorGrid = new Color[rows][cols];

    // Initialization of cells 
    for (int row = 0; row < rows; row++) {
      for (int col = 0; col < cols; col++) {
        ColorGrid[row][col] = null;
      } 
    }
  }
  @Override
  public int rows() {
    return this.rows;
  }

  @Override
  public int cols() {
    return this.cols;
  }

  @Override
  public List<CellColor> getCells() {
    List<CellColor> cellColors = new ArrayList<>();

    for (int row = 0; row < rows; row++) {
      for (int col = 0; col < cols; col++) {
        Color color = ColorGrid[row][col];

        CellPosition pos = new CellPosition(row, col);
        CellColor cellColor = new CellColor(pos, color);
        cellColors.add(cellColor);
      }
    }
    return cellColors;
  }

  @Override
  public Color get(CellPosition pos) {
    int row = pos.row();
    int col = pos.col();

    if (!isValidPosition(row, col)) {
      throw new IndexOutOfBoundsException("Cell position is out of bounds");
    }

    return ColorGrid[row][col];
  }

  @Override
  public void set(CellPosition pos, Color color) {
    int row = pos.row();
    int col = pos.col();
    
    if (!isValidPosition(row, col)) {
      throw new IndexOutOfBoundsException("Cell position is out of bounds");
    }

    ColorGrid[row][col] = color;

  }
  
  public int getRow() {
    throw new UnsupportedOperationException("Unimplemented method 'set'");
  }

  private boolean isValidPosition(int row, int col) {
    // Return true if both the length and width are positive and smaller than grid sizes
    return row >= 0 && row < this.ColorGrid.length && col >= 0 && col < this.ColorGrid[0].length;
  }
  
}

